##############################################################################
#   INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN  #
#                                                                            #
# 'reportable' flag not set during run                                       #
# 465.tonto (base) did not have enough runs!                                 #
# 453.povray (base) did not have enough runs!                                #
# 444.namd (base) did not have enough runs!                                  #
# 416.gamess (base) did not have enough runs!                                #
# 436.cactusADM (base) did not have enough runs!                             #
# 459.GemsFDTD (base) did not have enough runs!                              #
# 450.soplex (base) did not have enough runs!                                #
# 437.leslie3d (base) did not have enough runs!                              #
# 447.dealII (base) did not have enough runs!                                #
# 481.wrf (base) did not have enough runs!                                   #
# 433.milc (base) did not have enough runs!                                  #
# 410.bwaves (base) did not have enough runs!                                #
# 435.gromacs (base) did not have enough runs!                               #
# 482.sphinx3 (base) did not have enough runs!                               #
# 434.zeusmp (base) did not have enough runs!                                #
# 470.lbm (base) did not have enough runs!                                   #
# 454.calculix (base) did not have enough runs!                              #
# Unknown flags were used! See                                               #
#      http://www.spec.org/cpu2006/Docs/runspec.html#flagsurl                #
# for information about how to get rid of this error.                        #
#                                                                            #
#   INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN  #
##############################################################################
                           SPEC(R) CFP2006 Summary
                                    -- --
                           Thu Sep 28 22:45:05 2017

CPU2006 License: --                                      Test date: Sep-2017
Test sponsor: --                             Hardware availability: --      
Tested by:    --                             Software availability: --      

                                  Estimated                       Estimated
                Base     Base       Base        Peak     Peak       Peak
Benchmarks      Ref.   Run Time     Ratio       Ref.   Run Time     Ratio
-------------- ------  ---------  ---------    ------  ---------  ---------
410.bwaves                                  NR                                 
416.gamess                                  NR                                 
433.milc                                    NR                                 
434.zeusmp                                  NR                                 
435.gromacs                                 NR                                 
436.cactusADM                               NR                                 
437.leslie3d                                NR                                 
444.namd         8020       1279       6.27 *                                  
447.dealII      11440       1006      11.4  *                                  
450.soplex       8340        727      11.5  *                                  
453.povray       5320        597       8.90 *                                  
454.calculix                                NR                                 
459.GemsFDTD                                NR                                 
465.tonto                                   NR                                 
470.lbm                                     NR                                 
481.wrf                                     NR                                 
482.sphinx3                                 NR                                 
==============================================================================
410.bwaves                                  NR                                 
416.gamess                                  NR                                 
433.milc                                    NR                                 
434.zeusmp                                  NR                                 
435.gromacs                                 NR                                 
436.cactusADM                               NR                                 
437.leslie3d                                NR                                 
444.namd         8020       1279       6.27 *                                  
447.dealII      11440       1006      11.4  *                                  
450.soplex       8340        727      11.5  *                                  
453.povray       5320        597       8.90 *                                  
454.calculix                                NR                                 
459.GemsFDTD                                NR                                 
465.tonto                                   NR                                 
470.lbm                                     NR                                 
481.wrf                                     NR                                 
482.sphinx3                                 NR                                 
 Est. SPECfp(R)_base2006                 --
 Est. SPECfp2006                                                    Not Run


                                   HARDWARE
                                   --------
            CPU Name: Intel Core (Broadwell)
 CPU Characteristics:  
             CPU MHz: --
                 FPU: --
      CPU(s) enabled: -1 cores, 2 chips, -1 cores/chip, -1 threads/core
    CPU(s) orderable: --
       Primary Cache: --
     Secondary Cache: --
            L3 Cache: --
         Other Cache: --
              Memory: 3.368 GB fixme: If using DDR3, format is:
                      'N GB (M x N GB nRxn PCn-nnnnnR-n, ECC)'
      Disk Subsystem: 30 GB  add more disk info here
      Other Hardware: --


                                   SOFTWARE
                                   --------
    Operating System: Scientific Linux CERN SLC release 6.9 (Carbon)
                      3.10.0-514.10.2.el7.x86_64
            Compiler: --
       Auto Parallel: No
         File System: ext4
        System State: Run level N (add definition here)
       Base Pointers: --
       Peak Pointers: Not Applicable
      Other Software: --


                                Platform Notes
                                --------------
     Sysinfo program /hs06/test/test_1/SPEC_CPU2006_v1.2/Docs/sysinfo
     $Rev: 6775 $ $Date:: 2011-08-16 #$ 8787f7622badcf24e01c368b1db4377c
     running on f2429109a5a6 Thu Sep 28 22:45:07 2017
    
     This section contains SUT (System Under Test) info as seen by
     some common utilities.  To remove or add to this section, see:
       http://www.spec.org/cpu2006/Docs/config.html#sysinfo
    
     From /proc/cpuinfo
        model name : Intel Core Processor (Broadwell)
           2 "physical id"s (chips)
           2 "processors"
        cores, siblings (Caution: counting these is hw and system dependent.  The
        following excerpts from /proc/cpuinfo might not be reliable.  Use with
        caution.)
           cpu cores : 1
           siblings  : 1
           physical 0: cores 0
           physical 1: cores 0
        cache size : 4096 KB
    
     From /proc/meminfo
        MemTotal:        3531712 kB
        HugePages_Total:       0
        Hugepagesize:       2048 kB
    
     /usr/bin/lsb_release -d
        Scientific Linux CERN SLC release 6.9 (Carbon)
    
     From /etc/*release* /etc/*version*
        redhat-release: Scientific Linux CERN SLC release 6.9 (Carbon)
        system-release: Scientific Linux CERN SLC release 6.9 (Carbon)
        system-release-cpe: cpe:/o:redhat:enterprise_linux:6.9:ga
    
     uname -a:
        Linux f2429109a5a6 3.10.0-514.10.2.el7.x86_64 #1 SMP Fri Mar 3 00:04:05 UTC
        2017 x86_64 x86_64 x86_64 GNU/Linux
    
    
     SPEC is set to: /hs06/test/test_1/SPEC_CPU2006_v1.2
        Filesystem     Type  Size  Used Avail Use% Mounted on
        /dev/vdb       ext4   30G  3.7G   25G  14% /hs06
    
     (End of data from sysinfo program)

                                General Notes
                                -------------
     C base flags: -O2  -fPIC -pthread
     C++ base flags: -O2  -fPIC -pthread
     Fortran base flags: -O2  -fPIC -pthread
     despite compiling with -m32, wrf needs wrf_data_header_size=8
     to read the unformatted data input file correctly
     This means that the default 64 bit compiler still expects 8 byte
     by default (at least with the 20060325 snapshot)

                              Base Unknown Flags
                              ------------------
   444.namd: "g++ -m32" (in CXX) "g++ -m32" (in LD)
             "-O2 -fPIC -pthread" (in CXXOPTIMIZE)

 447.dealII: "g++ -m32" (in CXX) "g++ -m32" (in LD)
             "-O2 -fPIC -pthread" (in CXXOPTIMIZE)

 450.soplex: "g++ -m32" (in CXX) "g++ -m32" (in LD)
             "-O2 -fPIC -pthread" (in CXXOPTIMIZE)

 453.povray: "g++ -m32" (in CXX) "g++ -m32" (in LD)
             "-O2 -fPIC -pthread" (in CXXOPTIMIZE)


    SPEC and SPECfp are registered trademarks of the Standard Performance
    Evaluation Corporation.  All other brand and product names appearing
    in this result are trademarks or registered trademarks of their
    respective holders.
##############################################################################
#   INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN  #
#                                                                            #
# 'reportable' flag not set during run                                       #
# 465.tonto (base) did not have enough runs!                                 #
# 453.povray (base) did not have enough runs!                                #
# 444.namd (base) did not have enough runs!                                  #
# 416.gamess (base) did not have enough runs!                                #
# 436.cactusADM (base) did not have enough runs!                             #
# 459.GemsFDTD (base) did not have enough runs!                              #
# 450.soplex (base) did not have enough runs!                                #
# 437.leslie3d (base) did not have enough runs!                              #
# 447.dealII (base) did not have enough runs!                                #
# 481.wrf (base) did not have enough runs!                                   #
# 433.milc (base) did not have enough runs!                                  #
# 410.bwaves (base) did not have enough runs!                                #
# 435.gromacs (base) did not have enough runs!                               #
# 482.sphinx3 (base) did not have enough runs!                               #
# 434.zeusmp (base) did not have enough runs!                                #
# 470.lbm (base) did not have enough runs!                                   #
# 454.calculix (base) did not have enough runs!                              #
# Unknown flags were used! See                                               #
#      http://www.spec.org/cpu2006/Docs/runspec.html#flagsurl                #
# for information about how to get rid of this error.                        #
#                                                                            #
#   INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN -- INVALID RUN  #
##############################################################################
-----------------------------------------------------------------------------
For questions about this result, please contact the tester.
For other inquiries, please contact webmaster@spec.org.
Copyright 2006-2017 Standard Performance Evaluation Corporation
Tested with SPEC CPU2006 v1.2.
Report generated on Fri Sep 29 00:51:28 2017 by CPU2006 ASCII formatter v6400.
