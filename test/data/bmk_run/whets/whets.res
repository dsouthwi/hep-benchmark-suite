
##########################################
Double Precision C/C++ Whetstone Benchmark

Calibrate
       0.00 Seconds          1   Passes (x 100)
       0.01 Seconds          5   Passes (x 100)
       0.08 Seconds         25   Passes (x 100)
       0.39 Seconds        125   Passes (x 100)
       1.91 Seconds        625   Passes (x 100)
       9.57 Seconds       3125   Passes (x 100)

Use 3263  passes (x 100)

          Double Precision C/C++ Whetstone Benchmark

Loop content                  Result              MFLOPS      MOPS   Seconds

N1 floating point     -1.12398255667393476      1363.373              0.046
N2 floating point     -1.12187079889294172      1326.455              0.331
N3 if then else        1.00000000000000000                6744.698    0.050
N4 fixed point        12.00000000000000000               33618.270    0.031
N5 sin,cos etc.        0.49902937281518261                  84.996    3.194
N6 floating point      0.99999987890802811       440.360              3.997
N7 assignments         3.00000000000000000                5162.205    0.117
N8 exp,sqrt etc.       0.75100163018453692                  56.677    2.142

MWIPS                                           3293.766              9.907


##########################################
Double Precision C/C++ Whetstone Benchmark

Calibrate
       0.00 Seconds          1   Passes (x 100)
       0.02 Seconds          5   Passes (x 100)
       0.07 Seconds         25   Passes (x 100)
       0.39 Seconds        125   Passes (x 100)
       1.90 Seconds        625   Passes (x 100)
       9.56 Seconds       3125   Passes (x 100)

Use 3268  passes (x 100)

          Double Precision C/C++ Whetstone Benchmark

Loop content                  Result              MFLOPS      MOPS   Seconds

N1 floating point     -1.12398255667393476      1361.032              0.046
N2 floating point     -1.12187079889294172      1326.537              0.331
N3 if then else        1.00000000000000000                6745.199    0.050
N4 fixed point        12.00000000000000000               35562.236    0.029
N5 sin,cos etc.        0.49902937281518261                  85.006    3.199
N6 floating point      0.99999987890802811       440.880              3.998
N7 assignments         3.00000000000000000                4987.706    0.121
N8 exp,sqrt etc.       0.75100163018453692                  56.349    2.157

MWIPS                                           3290.481              9.932

