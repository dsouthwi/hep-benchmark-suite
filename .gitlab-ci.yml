stages:
  - build-base-image
  - test-components
  - test-installation
  - build-qa-image
  - test-qa-image
  - promote-image
  - announce-promoted-image
 


############################################################
#####              BUILD IMAGES                        #####
############################################################

# Build the base image which is used to test installation and build the final hep-benchmark-suite image
.definition_build_image: &template_build_image
  image: # NB enable shared runners and do not specify a CI tag
    name: gitlab-registry.cern.ch/ci-tools/docker-image-builder # CERN version of the Kaniko image
    entrypoint: [""]
  script:
    # Prepare Kaniko configuration file
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    # Build and push the image from the Dockerfile at the root of the project.
    # To push to a specific docker tag, amend the --destination parameter, e.g. --destination $CI_REGISTRY_IMAGE:$CI_BUILD_REF_NAME
    # See https://docs.gitlab.com/ee/ci/variables/predefined_variables.html#variables-reference for available variables
    - /kaniko/executor --context $CONTEXT --dockerfile $DOCKERFILE $DESTINATIONS 

job_build_announcement_image:
   stage: build-base-image
   before_script:
       - export DOCKERFILE=$CI_PROJECT_DIR/docker-images/announcement/Dockerfile
       - export CONTEXT=$CI_PROJECT_DIR/docker-images/announcement
       - export DESTINATIONS="--destination $CI_REGISTRY_IMAGE/announcement:latest --destination $CI_REGISTRY_IMAGE/announcement:${CI_COMMIT_SHA:0:8}"
   <<: *template_build_image
   only:
     refs:
       - qa
     changes:
       - docker-images/announcement/*

job_build_base_image_cc7:
   stage: build-base-image
   before_script:
       - export DOCKERFILE=$CI_PROJECT_DIR/docker-images/hep-benchmark-base-cc7/Dockerfile
       - export CONTEXT=$CI_PROJECT_DIR/docker-images/hep-benchmark-base-cc7
       - export DESTINATIONS="--destination $CI_REGISTRY_IMAGE/hep-benchmark-suite-base-cc7:latest --destination $CI_REGISTRY_IMAGE/hep-benchmark-suite-base-cc7:${CI_COMMIT_SHA:0:8}"
   <<: *template_build_image
   only:
     refs:
       - qa
     changes:
       - docker-images/hep-benchmark-base-cc7/*

job_build_qa_image_cc7:
   stage: build-qa-image
   before_script:
       - export DOCKERFILE=$CI_PROJECT_DIR/docker-images/hep-benchmark-cc7/Dockerfile
       - export CONTEXT=$CI_PROJECT_DIR/
       - export DESTINATIONS="--destination $CI_REGISTRY_IMAGE/hep-benchmark-suite-cc7:${CI_COMMIT_BRANCH} --destination $CI_REGISTRY_IMAGE/hep-benchmark-suite-cc7:${CI_COMMIT_SHA:0:8}"
   <<: *template_build_image
   only:
      variables:
       - $CI_COMMIT_BRANCH =~ /^qa-.*$/
       - $CI_COMMIT_BRANCH =~ /^qa$/

############################################################
#####              PROMOTE PROD IMAGES                 #####
############################################################

job_promote_prod_image_cc7:
   stage: promote-image
   image: $CI_REGISTRY_IMAGE/hep-benchmark-suite-cc7:qa
   tags:
     -  hep-benchmark-suite-docker-runner 
   before_script:
       - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN gitlab-registry.cern.ch
   after_script:
        - docker images
        - docker ps -a
   script: 
        - echo "current commit is ${CI_COMMIT_SHA:0:8}"
        - echo "current tag is ${CI_COMMIT_TAG}"
        - docker tag  $CI_REGISTRY_IMAGE/hep-benchmark-suite-cc7:qa $CI_REGISTRY_IMAGE/hep-benchmark-suite-cc7:${CI_COMMIT_TAG}
        - docker tag  $CI_REGISTRY_IMAGE/hep-benchmark-suite-cc7:qa $CI_REGISTRY_IMAGE/hep-benchmark-suite-cc7:latest
        - docker push $CI_REGISTRY_IMAGE/hep-benchmark-suite-cc7:${CI_COMMIT_TAG}
        - docker push $CI_REGISTRY_IMAGE/hep-benchmark-suite-cc7:latest
   only:
     variables:
       - $CI_COMMIT_TAG =~ /^v.*$/

job_announce_prod_image:
   stage: announce-promoted-image
   image: $CI_REGISTRY_IMAGE/announcement:latest
   only:
     variables:
       - $CI_COMMIT_TAG =~ /^v.*$/
   script: 
     - export IMAGE=$CI_REGISTRY_IMAGE/hep-benchmark-suite-cc7:${CI_COMMIT_TAG}
     - /announce.sh $IMAGE

############################################################
#####              TEST INSTALLATION                   #####
############################################################

.definition_test_installation: &template_test_installation
    # This job installs the hep-benchmark-suite using "make" 
    # then runs a configurable set of benchmarks 
    stage: test-installation
    image: $CI_REGISTRY_IMAGE/hep-benchmark-suite-base-cc7:latest
    script: 
      - export BMK_VOLUME=/scratch/jobs/${CI_JOB_NAME}_${CI_JOB_ID}
      - export BMK_RUNDIR=${BMK_VOLUME}/hep-benchmark-suite
      - export AMQ_ARGUMENTS="--queue_host=$QUEUE_HOST --queue_port=$QUEUE_PORT --username=$QUEUE_USERNAME --password=$QUEUE_PASSWD --topic=$QUEUE_TOPIC"
      - if [[ -z $BMK_HEPSCORE_CONF ]]; then
          export BMK_HEPSCORE_CONF=/opt/hep-benchmark-suite/scripts/hepscore/hepscore_ci.yaml;
        fi
      - $CI_PROJECT_DIR/test/test_installation_and_run.sh
    after_script:
      - export BMK_VOLUME=/scratch/jobs/${CI_JOB_NAME}_${CI_JOB_ID}
      - export BMK_RUNDIR=${BMK_VOLUME}/hep-benchmark-suite
      - tar -czf hep-benchmark-suite-test-$CI_JOB_ID.tgz ${BMK_RUNDIR} /opt/hep-benchmark-suite 
      - rm -rf ${BMK_VOLUME}
    only:
     variables:
       - $CI_COMMIT_BRANCH =~ /^qa-.*$/
       - $CI_COMMIT_BRANCH =~ /^qa$/
    artifacts:
        paths:
           - hep-benchmark-suite-test-$CI_JOB_ID.tgz
        expire_in: 1 week
        when: always

job_test_installation_cc7: 
   before_script:
       - export BMKLIST=DB12
   <<: *template_test_installation

job_test_kv_run_cc7:
   tags:
     -  hep-benchmark-suite-docker-runner 
   before_script:
       - export BMKLIST="DB12,kv"
   <<: *template_test_installation

job_test_hepscore_run_cc7:
   tags:
     -  hep-benchmark-suite-docker-runner 
   before_script:
       - export BMKLIST="hepscore"
   <<: *template_test_installation

# FIXME, does not work but covers a case not often used
.job_test_hepscore_run_cc7_singularity:
   tags:
     -  hep-benchmark-suite-docker-runner 
   before_script:
       - export BMKLIST="hepscore"
       - export BMK_HEPSCORE_CONF=/opt/hep-benchmark-suite/scripts/hepscore/hepscore_ci_singularity.yaml
   <<: *template_test_installation

job_test_hs06_run_cc7:
   tags:
     -  hep-benchmark-suite-docker-runner 
   before_script:
       - export BMKLIST="hs06_32"
       - rm -f /scratch/HEPSPEC/SPEC_CPU2006_v1.2/VERIFIED_INSTALLATION
   <<: *template_test_installation
        

job_test_spec2017_run_cc7:
   tags:
     -  hep-benchmark-suite-docker-runner 
   before_script:
       - export BMKLIST="spec2017"
   <<: *template_test_installation


############################################################
#####              TEST COMPONENTS                     #####
############################################################

.definition_bats_tests: &template_bats_tests
   stage: test-components
   image: $CI_REGISTRY_IMAGE/hep-benchmark-suite-cc7:latest
   script:
       - yum install -y skopeo
       - $CI_PROJECT_DIR/test/run_bats_tests.sh
   except:
      refs: 
        - master
        - qa

bats_tests_any_branch:
   <<: *template_bats_tests

bats_tests_qa:
   <<: *template_bats_tests
   script:
       - bats $CI_PROJECT_DIR/test/qa_send_queue.bat
   except:
      refs: 
        - master
   only:
      refs:
        - qa     

############################################################
#####              TEST SINGULARITY                    #####
############################################################

.definition_test_qa_singularity: &template_test_qa_singularity
    # This job tests the hep-benchmark-suite
    # running a configurable set of benchmarks
    stage: test-qa-image
    image: $CI_REGISTRY_IMAGE/hep-benchmark-suite-cc7:qa
    script:
      - if [[ -z $BMK_SUITE_IMAGE ]]; then
        export BMK_SUITE_IMAGE=$CI_REGISTRY_IMAGE/hep-benchmark-suite-cc7:qa;
        fi
      - export AMQ_ARGUMENTS="--queue_host=$QUEUE_HOST --queue_port=$QUEUE_PORT --username=$QUEUE_USERNAME --password=$QUEUE_PASSWD --topic=$QUEUE_TOPIC"
      - export BMK_VOLUME=/scratch/jobs/${CI_JOB_NAME}_${CI_JOB_ID}
      - export BMK_RUNDIR=${BMK_VOLUME}/hep-benchmark-suite
      - export SINGULARITY_CACHEDIR=${BMK_VOLUME}/singularity_cache
      - $CI_PROJECT_DIR/test/test_singularity_in_singularity_in_docker.sh
    after_script:
      - export BMK_VOLUME=/scratch/jobs/${CI_JOB_NAME}_${CI_JOB_ID}
      - export BMK_RUNDIR=${BMK_VOLUME}/hep-benchmark-suite
      - tar -czf hep-benchmark-suite-test-$CI_JOB_ID.tgz $BMK_RUNDIR /opt/hep-benchmark-suite 
      - rm -rf ${BMK_VOLUME}
    only:
     variables:
       - $CI_COMMIT_BRANCH =~ /^qa-.*$/
       - $CI_COMMIT_BRANCH =~ /^qa$/
    artifacts:
        paths:
           - hep-benchmark-suite-test-$CI_JOB_ID.tgz
        expire_in: 1 week
        when: always

job_test_sing_in_sing_hepscore:
    tags:
      -  hep-benchmark-suite-docker-runner
    before_script:
        - export BMKLIST="hepscore"
        - export BMK_HEPSCORE_CONF=/opt/hep-benchmark-suite/scripts/hepscore/hepscore_ci_singularity.yaml
    <<: *template_test_qa_singularity

job_test_sing_in_sing_hs06:
    tags:
      -  hep-benchmark-suite-docker-runner
    before_script:
      - export BMKLIST="hs06_32"
      - rm -f /scratch/HEPSPEC/SPEC_CPU2006_v1.2/VERIFIED_INSTALLATION
    <<: *template_test_qa_singularity

job_test_sing_in_sing_spec2017:
    tags:
      -  hep-benchmark-suite-docker-runner
    before_script:
        - export BMKLIST="spec2017"
    <<: *template_test_qa_singularity
